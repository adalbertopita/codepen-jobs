import React, { Component } from 'react';

class Header extends Component {
  
  render() {
    return (
      <nav className="navbar navbar-fixed-top navbar-dark bg-inverse">
        <div className="container">
          <a className="navbar-brand" href="#">{this.props.title}</a>
        </div>
      </nav>
    );
  }
}


export default Header;
