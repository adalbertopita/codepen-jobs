import React, { Component } from 'react';

class JobItem extends Component {
  render() {
    const {company_name, title, address, url} = this.props.job;
    let add;
    address ? add = address.address1 +' - '+address.city +' - '+address.state_prov+ ' / '+address.country : add = '';
    return (
        <a href={url} className="list-group-item list-group-item-action">
            <strong className="list-group-item-heading">{company_name}</strong>
            <h6 className="list-group-item-heading">{title}</h6>
            <p className="list-group-item-text">{add}</p>
        </a>
    );
  }
}

export default JobItem;