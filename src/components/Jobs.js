import React, { Component } from 'react';
import JobItem from "./JobItem";

class Jobs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data : [],
            url : this.props.url
        };
    }

    fetchData(){
        fetch(this.state.url)  
        .then(response => response.json()) 
        .then(result => {
            this.setState({data: result.jobs});
        })
        .catch(err => {
            console.error('Failed retrieving information', err);
        });
    }

    componentDidMount() {
        this.fetchData(this.state.url);
    }
  
    render() {
        return (
            <div className="wrapper">
                <div className="container">
                    <div className="row">
                        <div className="list-group">
                            {this.state.data.map((job, x) => <JobItem key={x} job={job}/>)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default Jobs;
