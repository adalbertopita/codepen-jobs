import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Header from "./components/Header";
import Jobs from "./components/Jobs";

import './index.css';

let json_url = "http://codepen.io/jobs.json"

class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <Header title="CodePen Jobs"/>
        <Jobs url={this.props.url}/>
      </div>
    );
  }
}

ReactDOM.render(
  <App url={json_url}/>,
  document.getElementById('root')
);
